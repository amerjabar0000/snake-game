# Snake Game Project

#### Stack used: React.js, HTML5 and CSS.
#### Hosted on: Azure cloud
#### Link: https://reactsnakegameproject.azurewebsites.net

This project was build in React.js framework as one single page. A Circular-Doubly Linked List data structure is implemented in order to optimize the performance by adding next elements of transition to the end of the list which is also the beginning of the list. This gives us the equal performance status from both directions.


### Project can be launched either from the link above, or cloned from this repository and launched as any React.js app.

```
git clone https://gitlab.com/amerjabar0000/snake-game
cd snake-game
npm run dev
```
