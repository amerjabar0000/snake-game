import { Component } from 'react';

import generateFood from '../helper/generateFood';
import { 
  upTurn,
  downTurn,
  leftTurn,
  rightTurn
} from '../helper/directionSelections';
import updateDirection from '../helper/updateDirection';
import errorHandler from '../helper/errorHandler';
import snakeEatsFood from '../helper/snakeEatsFood';
import { snakeGenerator, tableGenerator } from '../helper/stateGenerators';

import '../styles/App.css';

const Y_AXES = 10;
const X_AXES = 10;

export default class SnakeGame extends Component {

  constructor(props) {
    super(props);
    this.keyboardPressAction = this.keyboardPressAction.bind(this);
    
    this.state = {
      snakeBody: null,
      food: null,
      table: null,  
      updateInterval: -1,
      movementLocked: false,
      snakeSpeed: 1000,
      snakeSpeedInterval: -1,
      score: 0
    }
  }

  keyboardPressAction(target) {
    const direction = target.code;

    const { movementLocked, snakeSpeed } = this.state;
    let intervalId = null;
    if ( movementLocked )
      return;

    switch ( direction ) {
      case 'ArrowUp': upTurn(this).then(() => {
        this.setState({ movementLocked: true });
        intervalId = setInterval( async () => {
          updateDirection(this, X_AXES, Y_AXES)
          .then(({ snakeBody, nodeToRemove }) => {
            const tempTable = this.state.table;
            tempTable[nodeToRemove.y][nodeToRemove.x] = 0;
            // snakeEatsFood(this, Y_AXES, X_AXES);

            this.setState({
              snakeBody,
              table: tempTable,
              updateInterval: intervalId, 
              movementLocked: false
            });
          }).catch(err => {
            errorHandler(err, this.state.updateInterval === -1 ? intervalId : this.state.updateInterval);
          })
        }, snakeSpeed)
        }).catch(err => errorHandler(err, this.state.updateInterval))
        break;

      case 'ArrowDown': downTurn(this).then(() => {
        this.setState({ movementLocked: true });
        intervalId = setInterval( async () => {
          updateDirection(this, X_AXES, Y_AXES)
          .then(({ snakeBody, nodeToRemove }) => {
            const tempTable = this.state.table;
            tempTable[nodeToRemove.y][nodeToRemove.x] = 0;
            // snakeEatsFood(this, Y_AXES, X_AXES);

            this.setState({
              snakeBody,
              table: tempTable,
              updateInterval: intervalId, 
              movementLocked: false
            });
          }).catch(err => {
            errorHandler(err, this.state.updateInterval === -1 ? intervalId : this.state.updateInterval);
          })
        }, snakeSpeed)
        }).catch(err => errorHandler(err, this.state.updateInterval))
        break;

      case 'ArrowLeft': leftTurn(this).then(() => {
        this.setState({ movementLocked: true });
        intervalId = setInterval( async () => {
          updateDirection(this, X_AXES, Y_AXES)
          .then(({ snakeBody, nodeToRemove }) => {
            const tempTable = this.state.table;
            tempTable[nodeToRemove.y][nodeToRemove.x] = 0;
            // snakeEatsFood(this, Y_AXES, X_AXES);

            this.setState({
              snakeBody,
              table: tempTable,
              updateInterval: intervalId, 
              movementLocked: false
            });
          }).catch(err => {
            errorHandler(err, this.state.updateInterval === -1 ? intervalId : this.state.updateInterval);
          })
        }, snakeSpeed)
        }).catch(err => errorHandler(err, this.state.updateInterval))
        break;

      case 'ArrowRight': rightTurn(this).then(() => {
        this.setState({ movementLocked: true });
        intervalId = setInterval(() => {
          updateDirection(this, X_AXES, Y_AXES)
          .then(({ snakeBody, nodeToRemove }) => {
            const tempTable = this.state.table;
            tempTable[nodeToRemove.y][nodeToRemove.x] = 0;
            // snakeEatsFood(this, Y_AXES, X_AXES);

            this.setState({
              snakeBody,
              table: tempTable,
              updateInterval: intervalId, 
              movementLocked: false
            });
          }).catch(err => {
            errorHandler(err, this.state.updateInterval === -1 ? intervalId : this.state.updateInterval);
          })
        }, snakeSpeed)
        }).catch(err => errorHandler(err, this.state.updateInterval))
        break;

      default:
        break;
    }

    // if ( this.state.snakeSpeedInterval === -1 )
    //   this.setState({ snakeSpeedInterval: 0 }, () => {
    //     setInterval(() => this.setState({ snakeSpeed: this.state.snakeSpeed - (this.state.snakeSpeed / 4) }), 30000);
    //   });
  }

  componentDidMount() {
    const snakeBody = snakeGenerator(Y_AXES, X_AXES);
    const food = generateFood(snakeBody, Y_AXES, X_AXES);
    const table = tableGenerator(snakeBody, food, Y_AXES, X_AXES);
    this.setState({ snakeBody, food, table })
  }

  render() {

    return (
      <main tabIndex='0' onKeyDown={(event) => this.keyboardPressAction(event)}>
        <h2>{ this.state.score }</h2>
        <table>
          {
            this.state.table ? (
              this.state.table.map(ySet => (<tr>
                { ySet.map(xCell => <td
                    style={{ background: xCell === 1 
                      ? 'black' 
                      : xCell === 2 
                      ? 'green' 
                      : 'white'
                  }}></td>) }
              </tr>))
            ) : <></>
          }
        </table>
      </main>
    )
  }

}