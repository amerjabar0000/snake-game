import generateFood from "./generateFood";

const snakeEatsFood = (mainComponent, Y_AXES, X_AXES) => {
    const {
        head: { x: snakeX, y: snakeY },
        food: { x: foodX, y: foodY },
        score,
        snakeBody
    } = mainComponent.state;

    if ( snakeX === foodX && snakeY === foodY ) {
        const newFoodPosition = generateFood({ x: snakeX, y: snakeY }, Y_AXES, X_AXES)
        mainComponent.setState({ food: newFoodPosition, score: score + 1 })
    }

}

export default snakeEatsFood;