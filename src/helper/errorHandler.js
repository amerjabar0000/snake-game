const errorHandler = (errMessage, intervalId) => {
    if ( errMessage === 'Snake Hit The Wall' ) {
        alert('You Lose')
        clearInterval(intervalId);
    }
}

export default errorHandler;