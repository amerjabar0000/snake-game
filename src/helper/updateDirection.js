const updateDirection = (snakeDestination, snakeBody, tableSizeY, tableSizeX) => new Promise((resolver, rejector) => {
    let coppiedSnakeBody = snakeBody
    const { x, y } = coppiedSnakeBody.firstItem();
    
    let removedNode;
    console.log({ x, y });

    switch ( snakeDestination ) {
        case 'Up':
            if ( y === 0 ) {
                rejector('Snake Hit The Wall');
                return;
            }

            coppiedSnakeBody.add({
                x, y: y - 1
            });
            removedNode = coppiedSnakeBody.removeLast();

            resolver({
                snakeBody: coppiedSnakeBody,
                nodeToRemove: removedNode
            })
            break;

        case 'Down':
            if ( y === tableSizeY - 1 ) {
                rejector('Snake Hit The Wall');
                return;
            }

            coppiedSnakeBody.add({
                x, y: y + 1
            });
            removedNode = coppiedSnakeBody.removeLast();

            resolver({
                snakeBody: coppiedSnakeBody,
                nodeToRemove: removedNode
            })
           break;

        case 'Left':
            if ( x === 0 ) {
                rejector('Snake Hit The Wall');
                return;
            }

            coppiedSnakeBody.add({
                x: x - 1, y
            });
            removedNode = coppiedSnakeBody.removeLast();

            resolver({
                snakeBody: coppiedSnakeBody,
                nodeToRemove: removedNode
            })
            break;

        case 'Right':
            if ( x === tableSizeX - 1 ) {
                rejector('Snake Hit The Wall');
                return;
            }

            coppiedSnakeBody.add({
                x: x + 1, y
            });
            removedNode = coppiedSnakeBody.removeLast();

            resolver({
                snakeBody: coppiedSnakeBody,
                nodeToRemove: removedNode
            })
            break;

        default:
            break;
    }
})


export default updateDirection;