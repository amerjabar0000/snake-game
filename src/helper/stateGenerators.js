import LinkedList from "../lib/LinkedList";

export const tableGenerator = function(snakeBody, food, Y_AXES, X_AXES) {
    const cellsTable = Array.from({ length: Y_AXES })
        .map((_, index) => (
        Array.from({ length: X_AXES }).map((_, index) => 0)
    ));
    snakeBody.toArray().forEach(({ x, y }) => cellsTable[y][x] = 1)

    const { x, y } = food;
    cellsTable[y][x] = 2
    return cellsTable; 
}

export const snakeGenerator = function(Y_AXES, X_AXES) {
    const list = new LinkedList()
    list.add({ x: 4, y: 4 });
    list.add({ x: 4, y: 5 });
    list.add({ x: 4, y: 6 });
    // list.add(randomPositionGenerator(Y_AXES, X_AXES))
    return list;
}

export const randomPositionGenerator = function(Y_AXES, X_AXES) {
    const positions = {
        x: Number((Math.random() * (X_AXES - 1)).toFixed()),
        y: Number((Math.random() * (Y_AXES - 1)).toFixed()),    
    };

    return positions;
}