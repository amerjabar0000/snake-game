const generateFood = function(snakeBody, tableSizeY, tableSizeX) {
    const snakeBodyArray = snakeBody.toArray();

    let y;
    let x;

    snakeBodyArray.forEach(({ x: snakeX, y: snakeY }) => {
        do {
            x = Number((Math.random() * (tableSizeY - 1)).toFixed());
            y = Number((Math.random() * (tableSizeX - 1)).toFixed());
        } while ( x === snakeX && y === snakeY );
    })

    return ({ y, x });
}

export default generateFood;