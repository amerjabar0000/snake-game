export const upTurn = (mainComponent) => new Promise((resolver, rejector) => {
    const { state: { destination, updateInterval } } = mainComponent;
    if ( destination === 'Up' || destination === 'Down' ) {
        rejector('Contradictory Direction');
        return;
    }
    
    clearInterval(updateInterval);

    const prevState = mainComponent.state;
    mainComponent.setState({
        ...prevState,
        destination: 'Up',
        updateInterval: -1
    })

    resolver('State Updated')
})

export const downTurn = (mainComponent) => new Promise((resolver, rejector) => {
    const { state: { destination, updateInterval } } = mainComponent;
    if ( destination === 'Up' || destination === 'Down' ) {
        rejector('Contradictory Direction');
        return;
    }

    clearInterval(updateInterval);

    const prevState = mainComponent.state;
    mainComponent.setState({
        ...prevState,
        destination: 'Down',
        updateInterval: -1
    })

    resolver('State Updated')
})

export const leftTurn = (mainComponent) => new Promise((resolver, rejector) => {
    const { state: { destination, updateInterval } } = mainComponent;
    if ( destination === 'Left' || destination === 'Right' ) {
        rejector('Contradictory Direction');
        return;
    }

    clearInterval(updateInterval);

    const prevState = mainComponent.state;
    mainComponent.setState({
        ...prevState,
        destination: 'Left',
        updateInterval: -1
    })

    resolver('State Updated')
})

export const rightTurn = (mainComponent) => new Promise((resolver, rejector) => {
    const { state: { destination, updateInterval } } = mainComponent;
    if ( destination === 'Right' || destination === 'Left' ) {
        rejector('Contradictory Direction');
        return;
    }

    clearInterval(updateInterval);

    const prevState = mainComponent.state;
    mainComponent.setState({
        ...prevState,
        destination: 'Right',
        updateInterval: -1
    })

    resolver('State Updated')
})